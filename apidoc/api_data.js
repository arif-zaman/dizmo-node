define({ "api": [
  {
    "type": "delete",
    "url": "/apply/:id",
    "title": "Delete Specific Job Application",
    "name": "DeleteApplication",
    "group": "Application",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Job Application unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ApplyController.js",
    "groupTitle": "Application"
  },
  {
    "type": "get",
    "url": "/apply/page/:pageNum/:pageSize",
    "title": "Request All Job Application information",
    "name": "GetApplicationList",
    "group": "Application",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Size of Each Page.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Application Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Empolyee ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "jobID",
            "description": "<p>Job ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ApplyController.js",
    "groupTitle": "Application"
  },
  {
    "type": "get",
    "url": "/apply/employee/:id",
    "title": "Request All Job Application Information of a Specific Employee",
    "name": "GetApplicationListofEmployee",
    "group": "Application",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employee Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Application Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Empolyee ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "jobID",
            "description": "<p>JobID ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ApplyController.js",
    "groupTitle": "Application"
  },
  {
    "type": "get",
    "url": "/apply/job/:id",
    "title": "Request All Job Application Information of a Specific Job",
    "name": "GetApplicationListofJob",
    "group": "Application",
    "permission": [
      {
        "name": "Employer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Job Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Application Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Empolyee ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "jobID",
            "description": "<p>JobID ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ApplyController.js",
    "groupTitle": "Application"
  },
  {
    "type": "post",
    "url": "/apply",
    "title": "Create New Job Application",
    "name": "PostApplication",
    "group": "Application",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "jobID",
            "description": "<p>Mandatory Job ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Mandatory Employee ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ApplyController.js",
    "groupTitle": "Application"
  },
  {
    "type": "delete",
    "url": "/category/:id",
    "title": "Delete Specific Category",
    "name": "DeleteCategory",
    "group": "Category",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Category unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/CategoryController.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/category/:id",
    "title": "Request a Specific Category Info",
    "name": "GetCategory",
    "group": "Category",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Category Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Category Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Category.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/CategoryController.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/category/",
    "title": "Request All Category List",
    "name": "GetCategoryList",
    "group": "Category",
    "permission": [
      {
        "name": "None"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Category Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Category.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/CategoryController.js",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "/category",
    "title": "Create New Category",
    "name": "PostCategory",
    "group": "Category",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Name",
            "description": "<p>Mandatory Category Name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/CategoryController.js",
    "groupTitle": "Category"
  },
  {
    "type": "put",
    "url": "/category/:id",
    "title": "Update Specific Category",
    "name": "UpdateCategory",
    "group": "Category",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Category unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Optional Category Name.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/CategoryController.js",
    "groupTitle": "Category"
  },
  {
    "type": "delete",
    "url": "/chat/:id",
    "title": "Delete Specific Chat History",
    "name": "DeleteChat",
    "group": "Chat",
    "permission": [
      {
        "name": "User"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Chat's unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ChatController.js",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/chat",
    "title": "Request All Chat History",
    "name": "GetChatHistory",
    "group": "Chat",
    "permission": [
      {
        "name": "User"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employee/Employer ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Chat Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerID",
            "description": "<p>Employer ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Employer ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Chat Created Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ChatController.js",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/chat",
    "title": "Add New Message",
    "name": "PostChat",
    "group": "Chat",
    "permission": [
      {
        "name": "User"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mandatory Message Body</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employerID",
            "description": "<p>Mandatory Employer ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Mandatory Employee ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/ChatController.js",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/employee/:id",
    "title": "Request a Specific Employee information",
    "name": "GetEmployee",
    "group": "Employee",
    "permission": [
      {
        "name": "User"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employee's unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Employee's unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>firstname of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>lastname of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userID",
            "description": "<p>Employee's unique User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the Employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": "<p>Website of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description about the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "URL",
            "optional": false,
            "field": "coverphoto",
            "description": "<p>URL of Cover Photo of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "URL",
            "optional": false,
            "field": "propic",
            "description": "<p>URL of Profile Pictures of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "URL",
            "optional": false,
            "field": "gender",
            "description": "<p>Gender of the Employee (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Employee Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/EmployeeController.js",
    "groupTitle": "Employee"
  },
  {
    "type": "get",
    "url": "/employee/page/:pageNum",
    "title": "Request All Employee information",
    "name": "GetEmployeeList",
    "group": "Employee",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Employee's unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userID",
            "description": "<p>Employee's unique User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the Employee.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/EmployeeController.js",
    "groupTitle": "Employee"
  },
  {
    "type": "put",
    "url": "/employee/:id",
    "title": "Update Specific Employee",
    "name": "UpdateEmployee",
    "group": "Employee",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employee's unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "firstname",
            "description": "<p>firstname of the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lastname",
            "description": "<p>lastname of the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "website",
            "description": "<p>Website of the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description about the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "country",
            "description": "<p>Country of the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "URL",
            "optional": true,
            "field": "coverphoto",
            "description": "<p>URL of Cover Photo of the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "URL",
            "optional": true,
            "field": "propic",
            "description": "<p>URL of Profile Pictures of the Employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "URL",
            "optional": true,
            "field": "gender",
            "description": "<p>Gender of the Employee.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/EmployeeController.js",
    "groupTitle": "Employee"
  },
  {
    "type": "get",
    "url": "/employer/:id",
    "title": "Request a Specific Employer information",
    "name": "GetEmployer",
    "group": "Employer",
    "permission": [
      {
        "name": "User"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employer's unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Employer's unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userID",
            "description": "<p>Employer's unique User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the Employer.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>name of the Employer (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": "<p>Website of the Employer (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "decription",
            "description": "<p>Description about the Employer (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country of the Employer (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "URL",
            "optional": false,
            "field": "coverphoto",
            "description": "<p>URL of Cover Photo of the Employer (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "URL",
            "optional": false,
            "field": "propic",
            "description": "<p>URL of Profile Pictures of the Employer (optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Employer Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/EmployerController.js",
    "groupTitle": "Employer"
  },
  {
    "type": "get",
    "url": "/employer/page/:pageNum",
    "title": "Request All Employer information",
    "name": "GetEmployerList",
    "group": "Employer",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Employer's unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userID",
            "description": "<p>Employer's unique User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the Employer.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/EmployerController.js",
    "groupTitle": "Employer"
  },
  {
    "type": "put",
    "url": "/employer/:id",
    "title": "Update Specific Employer",
    "name": "UpdateEmployer",
    "group": "Employer",
    "permission": [
      {
        "name": "Employer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employer's unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>name of the Employer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": "<p>Website of the Employer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "decription",
            "description": "<p>Description about the Employer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country of the Employer.</p>"
          },
          {
            "group": "Parameter",
            "type": "URL",
            "optional": false,
            "field": "coverphoto",
            "description": "<p>URL of Cover Photo of the Employer.</p>"
          },
          {
            "group": "Parameter",
            "type": "URL",
            "optional": false,
            "field": "propic",
            "description": "<p>URL of Profile Pictures of the Employer.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/EmployerController.js",
    "groupTitle": "Employer"
  },
  {
    "type": "delete",
    "url": "/faq/:id",
    "title": "Delete Specific Question",
    "name": "DeleteFaq",
    "group": "Faq",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Question's unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/FaqController.js",
    "groupTitle": "Faq"
  },
  {
    "type": "get",
    "url": "/faq",
    "title": "Request All FAQ",
    "name": "GetFaqList",
    "group": "Faq",
    "permission": [
      {
        "name": "None"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>FAQ Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "question",
            "description": "<p>Frequently Asked Question.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "answer",
            "description": "<p>Answers to the Questions</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/FaqController.js",
    "groupTitle": "Faq"
  },
  {
    "type": "post",
    "url": "/faq",
    "title": "Add New Questions",
    "name": "PostFaq",
    "group": "Faq",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "question",
            "description": "<p>Mandatory Question.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "answer",
            "description": "<p>Mandatory Answer to the Question.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/FaqController.js",
    "groupTitle": "Faq"
  },
  {
    "type": "put",
    "url": "/faq/:id",
    "title": "Update Specific Question",
    "name": "UpdateFaq",
    "group": "Faq",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Question's unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "question",
            "description": "<p>Question.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "answer",
            "description": "<p>Answer to the Question.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/FaqController.js",
    "groupTitle": "Faq"
  },
  {
    "type": "delete",
    "url": "/job/:id",
    "title": "Delete Specific Job",
    "name": "DeleteJob",
    "group": "Job",
    "permission": [
      {
        "name": "Employer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Job's unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "get",
    "url": "/job/:id",
    "title": "Request a Specific Job information",
    "name": "GetJob",
    "group": "Job",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>Job Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Job Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Job's Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subcategory",
            "description": "<p>Job's Sub-Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerName",
            "description": "<p>Employer Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerMail",
            "description": "<p>Employer E-mail.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Job Description optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "deadline",
            "description": "<p>Job Deadline(optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "featured",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Job Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "get",
    "url": "/job/page/:pageNum",
    "title": "Request All Job information",
    "name": "GetJobList",
    "group": "Job",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Job Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Job's Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subcategory",
            "description": "<p>Job's Sub-Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerName",
            "description": "<p>Employer Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerMail",
            "description": "<p>Employer E-mail.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Job Description optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "deadline",
            "description": "<p>Job Deadline(optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "featured",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Job Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "get",
    "url": "/job/category/:id",
    "title": "Request Job information of a Specific Category",
    "name": "GetJobofCategory",
    "group": "Job",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>Category Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Job Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Job's Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subcategory",
            "description": "<p>Job's Sub-Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerName",
            "description": "<p>Employer Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerMail",
            "description": "<p>Employer E-mail.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Job Description optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "deadline",
            "description": "<p>Job Deadline(optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "featured",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Job Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "get",
    "url": "/job/employer/:id",
    "title": "Request Job information of a Specific Employer",
    "name": "GetJobofEmployer",
    "group": "Job",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>Employer Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Job Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Job's Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subcategory",
            "description": "<p>Job's Sub-Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerName",
            "description": "<p>Employer Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerMail",
            "description": "<p>Employer E-mail.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Job Description optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "deadline",
            "description": "<p>Job Deadline(optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "featured",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Job Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "get",
    "url": "/job/subcategory/:id",
    "title": "Request Job information of a Specific SubCategory",
    "name": "GetJobofSubCategory",
    "group": "Job",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>Sub-Category Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Job Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Job's Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subcategory",
            "description": "<p>Job's Sub-Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerName",
            "description": "<p>Employer Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerMail",
            "description": "<p>Employer E-mail.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Job Description optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "deadline",
            "description": "<p>Job Deadline(optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "featured",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Job Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "get",
    "url": "/search/:keyword",
    "title": "Request a Specific Job information",
    "name": "GetSearchResults",
    "group": "Job",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>Job Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Job Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Job Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Job's Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subcategory",
            "description": "<p>Job's Sub-Category Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerName",
            "description": "<p>Employer Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employerMail",
            "description": "<p>Employer E-mail.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Job Description optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "deadline",
            "description": "<p>Job Deadline(optional).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "featured",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created",
            "description": "<p>Job Creation Date (Js Date Format).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated",
            "description": "<p>Info Update Date (Js Date Format).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "post",
    "url": "/job",
    "title": "Create New Job",
    "name": "PostJob",
    "group": "Job",
    "permission": [
      {
        "name": "Employer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Mandatory Job's Title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Mandatory Job Category ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subcategoryID",
            "description": "<p>Mandatory Job Sub-Category ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employerID",
            "description": "<p>Mandatory Employer ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Job's Descripton.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "deadline",
            "description": "<p>Deadline of the Job Application.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "put",
    "url": "/job/:id",
    "title": "Update Specific Job",
    "name": "UpdateJob",
    "group": "Job",
    "permission": [
      {
        "name": "Employer"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Job's unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Job's Title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Job's Descripton.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "deadline",
            "description": "<p>Deadline of the Job Application.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/JobController.js",
    "groupTitle": "Job"
  },
  {
    "type": "delete",
    "url": "/static/:id",
    "title": "Delete Specific Static Element",
    "name": "DeleteStatic",
    "group": "Static",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Attribute's unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/StaticController.js",
    "groupTitle": "Static"
  },
  {
    "type": "get",
    "url": "/static/:id",
    "title": "Request Given Static Elements",
    "name": "GetStatic",
    "group": "Static",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Static Page ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Static Page ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "attribute",
            "description": "<p>Static Page Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Static Page description.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/StaticController.js",
    "groupTitle": "Static"
  },
  {
    "type": "get",
    "url": "/static",
    "title": "Request All Static Elements",
    "name": "GetStaticList",
    "group": "Static",
    "permission": [
      {
        "name": "None"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Static Page ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "attribute",
            "description": "<p>Static Page Name.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/StaticController.js",
    "groupTitle": "Static"
  },
  {
    "type": "post",
    "url": "/static",
    "title": "Add New Static Pages",
    "name": "PostStatic",
    "group": "Static",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "attribute",
            "description": "<p>Mandatory Attribute Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Mandatory Attribute Description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/StaticController.js",
    "groupTitle": "Static"
  },
  {
    "type": "put",
    "url": "/static/:id",
    "title": "Update Specific Static Element",
    "name": "UpdateStatic",
    "group": "Static",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Attribute's unique ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "attribute",
            "description": "<p>Attribute Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/StaticController.js",
    "groupTitle": "Static"
  },
  {
    "type": "delete",
    "url": "/subcategory/:id",
    "title": "Delete Specific SubCategory",
    "name": "DeleteSubCategory",
    "group": "SubCategory",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employers unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubCategoryController.js",
    "groupTitle": "SubCategory"
  },
  {
    "type": "get",
    "url": "/subcategory/category/:id",
    "title": "Request All SubCategory of Specific subcategory",
    "name": "GetSpecificSubCategoryList",
    "group": "SubCategory",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Category Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>SubCategory unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the SubCategory.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubCategoryController.js",
    "groupTitle": "SubCategory"
  },
  {
    "type": "get",
    "url": "/subcategory/:id",
    "title": "Request a Specific SubCategory Info",
    "name": "GetSubCategory",
    "group": "SubCategory",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>SubCategory Unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>SubCategory unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the SubCategory.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubCategoryController.js",
    "groupTitle": "SubCategory"
  },
  {
    "type": "get",
    "url": "/subcategory",
    "title": "Request All SubCategory List",
    "name": "GetSubCategoryList",
    "group": "SubCategory",
    "permission": [
      {
        "name": "None"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>SubCategory unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the SubCategory.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubCategoryController.js",
    "groupTitle": "SubCategory"
  },
  {
    "type": "post",
    "url": "/subcategory",
    "title": "Create New SubCategory",
    "name": "PostSubCategory",
    "group": "SubCategory",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory Sub-Category Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Mandatory Category ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubCategoryController.js",
    "groupTitle": "SubCategory"
  },
  {
    "type": "put",
    "url": "/subcategory/:id",
    "title": "Update Specific SubCategory",
    "name": "UpdateSubCategory",
    "group": "SubCategory",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>SubCategory's unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Sub-Category Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubCategoryController.js",
    "groupTitle": "SubCategory"
  },
  {
    "type": "delete",
    "url": "/subscribe/:id",
    "title": "Delete Specific Subscription",
    "name": "DeleteSubscription",
    "group": "Subscribe",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Subscription unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubscribeController.js",
    "groupTitle": "Subscribe"
  },
  {
    "type": "get",
    "url": "/subscribe/page/:pageNum",
    "title": "Request All Subscription information",
    "name": "GetSubscriptionList",
    "group": "Subscribe",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Subscription Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Empolyee ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubscribeController.js",
    "groupTitle": "Subscribe"
  },
  {
    "type": "get",
    "url": "/subscribe/category/:id",
    "title": "Request All Subscription Information of a Category",
    "name": "GetSubscriptionListofCategory",
    "group": "Subscribe",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Category Unique ID Number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Subscription Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Empolyee ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubscribeController.js",
    "groupTitle": "Subscribe"
  },
  {
    "type": "get",
    "url": "/subscribe/employee/:id",
    "title": "Request All Subscription Information of an Employee",
    "name": "GetSubscriptionListofEmployee",
    "group": "Subscribe",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Employee Unique ID Number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Subscription Unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Empolyee ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Category ID.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubscribeController.js",
    "groupTitle": "Subscribe"
  },
  {
    "type": "post",
    "url": "/subscribe",
    "title": "Create New Subscription",
    "name": "PostSubscribe",
    "group": "Subscribe",
    "permission": [
      {
        "name": "Employee"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Mandatory Employee ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryID",
            "description": "<p>Mandatory Category ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/SubscribeController.js",
    "groupTitle": "Subscribe"
  },
  {
    "type": "delete",
    "url": "/user/:id",
    "title": "Delete Specific User",
    "name": "DeleteUser",
    "group": "User",
    "permission": [
      {
        "name": "User"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/UserController.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/page/:pageNum",
    "title": "Request All User information",
    "name": "GetUserList",
    "group": "User",
    "permission": [
      {
        "name": "Admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>Page Number For Pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>Each Page's Size.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Role of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "created",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "updated",
            "description": "<p>true/false.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/UserController.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Log In",
    "name": "LogIN",
    "group": "User",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory User's Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory User's Password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/LoginController.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/",
    "title": "Create New User",
    "name": "PostUser",
    "group": "User",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory User's Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory User's Password (Length at least 5).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Mandatory User's Role.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/UserController.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/:id/:hid",
    "title": "Verify User Email",
    "name": "VerifyUser",
    "group": "User",
    "permission": [
      {
        "name": "None"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hid",
            "description": "<p>Verification Key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>success/error message.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "controllers/UserController.js",
    "groupTitle": "User"
  }
] });
