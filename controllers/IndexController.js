var express = require('express')
var router 	= express.Router()


// ---------------------------------------------------------
// ********************* Home ******************************
// ---------------------------------------------------------


/* GET home page. */
router.get('/', function(req, res) {
  res.json({
  	message : "Welcome to Dizmo API."
  })
})


module.exports = router