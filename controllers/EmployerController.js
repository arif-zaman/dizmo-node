var express 	= require('express')
var router 		= express.Router()
var Employer 	= require('../models/EmployerModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// ******************** EMPLOYER ***************************
// ---------------------------------------------------------


/**
 * @api {get} /employer/page/:pageNum Request All Employer information
 * @apiName GetEmployerList
 * @apiGroup Employer
 * @apiPermission Admin
 *
 * @apiParam {Number} pageNum Page Number For Pagination.
 * @apiParam {Number} pageSize Each Page's Size.
 *
 * @apiSuccess {String} _id			Employer's unique ID.
 * @apiSuccess {String} userID 		Employer's unique User ID.
 * @apiSuccess {String} email 		Email of the Employer.
 */

router.get('/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Employer.find({}).sort({created:-1}).skip(skp).limit(lt)

	query.select('userID email')
	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Employer List.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /employer/:id Request a Specific Employer information
 * @apiName GetEmployer
 * @apiGroup Employer
 * @apiPermission User
 *
 * @apiParam {String} id Employer's unique ID.
 *
 * @apiSuccess {String} _id				Employer's unique ID.
 * @apiSuccess {String} userID 			Employer's unique User ID.
 * @apiSuccess {String} email 			Email of the Employer.
 * @apiSuccess {String} name  			name of the Employer (optional).
 * @apiSuccess {String} website  		Website of the Employer (optional).
 * @apiSuccess {String} decription		Description about the Employer (optional).
 * @apiSuccess {String} country  		Country of the Employer (optional).
 * @apiSuccess {URL} 	coverphoto 		URL of Cover Photo of the Employer (optional).
 * @apiSuccess {URL} 	propic 			URL of Profile Pictures of the Employer (optional).
 * @apiSuccess {Date} 	created 		Employer Creation Date (Js Date Format).
 * @apiSuccess {Date} 	updated  		Info Update Date (Js Date Format).
 */

router.get('/:id', function(req, res) {
	var ID = req.params.id
	var query = Employer.findOne({ _id:ID })

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Employer of Given ID",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {put} /employer/:id Update Specific Employer
 * @apiName UpdateEmployer
 * @apiGroup Employer
 * @apiPermission Employer
 *
 * @apiParam {String} id Employer's unique ID.
 *
 * @apiParam {String} name  			name of the Employer.
 * @apiParam {String} website  		Website of the Employer.
 * @apiParam {String} decription		Description about the Employer.
 * @apiParam {String} country  		Country of the Employer.
 * @apiParam {URL} 	coverphoto 		URL of Cover Photo of the Employer.
 * @apiParam {URL} 	propic 			URL of Profile Pictures of the Employer.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.put('/:id', function(req, res) {
	var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
		Employer.findOne({"_id" : ID}, function(err, employer){
			if (err) {
				res.status(500).json({
					success: false,
					message: "Internal Server Error, Update Failed!"
				})
			}
			
	        if (!employer){
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (req.body.name)
	        		employer.name = req.body.name

	        	if (req.body.website)
	        		employer.website = req.body.website

	        	if (req.body.description)
	        		employer.description = req.body.description

	        	if (req.body.coverphoto)
	        		employer.coverphoto = req.body.coverphoto

	        	if (req.body.propic)
	        		employer.propic = req.body.propic

	        	if (req.body.country)
	        		employer.country = req.body.country

	        	employer.updated = new Date()
	        	employer.save(function(error, data){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


module.exports = router