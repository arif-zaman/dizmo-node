var express 	= require('express')
var router 		= express.Router()
var Apply 		= require('../models/ApplyModel')
var Empolyee 	= require('../models/EmployeeModel')
var Job 		= require('../models/JobModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// ******************** APPLICANTS *************************
// ---------------------------------------------------------


/**
 * @api 		{post} 	/apply Create New Job Application
 * @apiName 	PostApplication
 * @apiGroup 	Application
 * @apiPermission 	Employee
 *
 * @apiParam 	{String} 	id  		Mandatory Unique ID. Automatically Added
 * @apiParam 	{String} 	jobID  		Mandatory Job ID.
 * @apiParam 	{String} 	applicantID	Mandatory Applicant's User ID (Must be Employee).
 * @apiParam 	{Date} 		created		Mandatory Creation TimeStamp. Automatically Added.
 *
 * @apiSuccess 	{Boolean} 	success		true/false.
 * @apiSuccess 	{String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.jobID)
		jobID = req.body.jobID

	if (req.body.applicantID)
		applicantID = req.body.applicantID

    if (!jobID || jobID.length != 24 || !applicantID || applicantID.length != 24){
		res.status(400).json({
			success : false,
			message : "EmployeeID and JobID length must be 24."
		})
	} else{
	    apply = new Apply()

	    Job.findOne({ _id:jobID }, function(err,job){
	    	if (err){
	    		res.status(500).json({
	    			success : false,
	    			message : "Internal Server Error!"
	    		})
	    	}

	    	if (!job){
	    		res.status(404).json({
	    			success : false,
	    			message : "Provides a Valid Employee."
	    		})
	    	}

	    	apply.jobID = jobID
	    })

	    Empolyee.findOne({ _id:applicantID }, function(err,emp){
	    	if (err){
	    		res.status(500).json({
	    			success : false,
	    			message : "Internal Server Error!"
	    		})
	    	}

	    	if (!emp){
	    		res.status(404).json({
	    			success : false,
	    			message : "Provides a Valid Employee."
	    		})
	    	}

	    	apply.applicantID = applicantID
	    })

	    apply.created = new Date()
	    apply.save(function(error, data){
	        if (!error){
	            res.json({
					success: true,
					message: "New Job Job Application created."
				})
	        } else {
	            res.status(500).json({
					success: false,
					message: "Internal Server Error! Combination of EmployeeID and JobID must be unique!"
				})
	        }
	    })
	}
})


/**
 * @api 		{get} /apply/page/:pageNum/:pageSize	Request All Job Application information
 * @apiName 	GetApplicationList
 * @apiGroup 	Application
 * @apiPermission 	Admin
 *
 * @apiParam 	{Number} 	pageNum 		Page Number For Pagination.
 * @apiParam 	{Number} 	pageSize 		Size of Each Page.
 *
 * @apiSuccess {String} 	id				Job Application Unique ID.
 * @apiSuccess {String} 	applicantID 	Applicant's User ID.
 * @apiSuccess {String} 	jobID			Job ID.
 */

router.get('/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Apply.find({}).sort({ jobID:1,created:-1}).skip(skp).limit(lt).select('jobID applicantID')
	
	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "List of Job Application.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api 		{get} 	/apply/user/:id 	Request All Job Application Information of a Specific User
 * @apiName 	GetApplicationListofEmployee
 * @apiGroup 	Application
 * @apiPermission 	Employee
 *
 * @apiParam 	{String} 	id 	Employee Unique ID.
 *
 * @apiSuccess 	{String} 	id				Job Application Unique ID.
 * @apiSuccess 	{String} 	applicantID 	Applicant's User ID.
 * @apiSuccess 	{String} 	jobID			JobID ID.
 */

router.get('/employee/:id/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Apply.find({ applicantID:req.params.id }).sort({created:-1}).skip(skp).limit(lt)

	query.select('jobID')
    query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "List of Job Application.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api 		{get} 	/apply/job/:id Request All Job Application Information of a Specific Job
 * @apiName 	GetApplicationListofJob
 * @apiGroup 	Application
 * @apiPermission 	Employer
 *
 * @apiParam 	{String} 	id 		Job Unique ID.
 *
 * @apiSuccess 	{String} 	_id				Job Application Unique ID.
 * @apiSuccess 	{String} 	applicantID 		Empolyee ID.
 * @apiSuccess 	{String} 	jobID			JobID ID.
 */

router.get('/job/:id/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Apply.find({ jobID:req.params.id }).sort({created:-1}).skip(skp).limit(lt)

	query.select('applicantID')
    query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "List of Job Application.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api 		{delete} 	/apply/:id Delete Specific Job Application
 * @apiName 	DeleteApplication
 * @apiGroup 	Application
 * @apiPermission 	Employee
 *
 * @apiParam 	{String} 	id 	Job Application unique ID.
 *
 * @apiSuccess 	{Boolean} 	success		true/false.
 * @apiSuccess 	{String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
  	var ID = req.params.id
    
    Apply.findOne({ _id:ID }, function(err, data){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error"
			});
		} else{
			if (data){
				data.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error"
						})
					} else{
						res.json({
							success : true,
							message : "Successfully Deleted!"
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
})


module.exports = router