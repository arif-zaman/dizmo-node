var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var FAQSchema = new Schema({
	question	: {type : String, trim: true, index : true, required : true, unique : true},
	answer 		: String,
	created 	: Date,
	updated 	: Date
});


module.exports 	= mongoose.model('FAQ',FAQSchema);