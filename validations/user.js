/**************** Redirect function ******************************************/
var Redirect = function(req, res){
    res.status(403).json({
		success: false,
		message: "Unauthorized Access!"
	})
}


/**************** Credentials Checks *****************************************/
var isRelevantUser = function(req, res){
    return true;
}


/**************** Login Verfication middlewares ******************************/
var isAuthenticated = function(req, res, next){
    if(isRelevantUser(req, res)){
        return next();
    }

    Redirect(req, res);
}


exports.isAuthenticated = isAuthenticated;